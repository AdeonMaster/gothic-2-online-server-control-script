# Gothic 2 Online Server Control Script

This project provides a control script for managing your server, allowing you to easily start, stop, and restart it on Windows/Linux.

### Linux Script Usage

1. Place the `scontrol.sh` script in your server's directory.
2. Make the script executable with the following command:

   ```bash
   chmod +x scontrol.sh
   ```

3. Run the script:

   ```bash
   ./scontrol.sh
   ```

### Windows Script Usage

1. Place the `scontrol.bat` script in your server's directory.
2. Run the script:

   ```bat
   ./scontrol.bat
   ```

## In-Game Control Example

To control the server from within the game, you can use the following example:

```js
addEventHandler("onPlayerCommand", function(pid, cmd, params) {
    switch (cmd) {
        case "stop":
            // Server shutdown
            exit(0)
            break

        case "restart":
            // Server restart
            exit(1)
            break
    }
});
```
