@echo off

:loop
G2O_Server.x64.exe

IF %ERRORLEVEL% EQU 0 (
    echo Server was stopped. Exiting script.
    pause
    exit
) ELSE IF %ERRORLEVEL% EQU 1 (
    echo Restarting server...
) ELSE (
    echo Unknown exit code: %ERRORLEVEL%. Exiting script.
    pause
    exit
)

for /l %%x in (1, 1, 3) do (
    echo %%x...
    timeout 1 > NUL
)

echo Rebooting now!
goto loop
